let favouritesMovies=require('./2-movies.cjs');

function basedOnActors(objectGiven){
    
    let result=Object.entries(objectGiven).filter((arrayComing)=>{
        return ((arrayComing[1].actors).includes("Leonardo Dicaprio"))
    });

    return Object.fromEntries(result);
}

let result=basedOnActors(favouritesMovies);

console.log(result)