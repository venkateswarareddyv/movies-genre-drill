let favouritesMovies=require('./2-movies.cjs');

function groupbyGenre(favouritesMovies){
    const genres = {
        drama: [],
        "sci-fi": [],
        adventure: [],
        thriller: [],
        crime: [],
      };
      
      const priority = ["drama", "sci-fi", "adventure", "thriller", "crime"];
      
      const groupedMovies = Object.keys(favouritesMovies).reduce((acc, movie) => {
        const movieGenres = favouritesMovies[movie].genre;
        const genre = priority.find((g) => movieGenres.includes(g));
        if (genre) {
          acc[genre].push(movie);
        }
        return acc;
      }, genres);
      
    return groupedMovies;
}

let result=groupbyGenre(favouritesMovies);

console.log(result);

