let favouritesMovies=require('./2-movies.cjs');

function moviesGreaterThan500M(objectGiven){
    
    let result=Object.entries(objectGiven).filter((arrayComing)=>{
        return (arrayComing[1].totalEarnings).replace("$","").replace("M","")>500
    });
    
    return Object.fromEntries(result);
}

let result=moviesGreaterThan500M(favouritesMovies);

console.log(result)