let favouritesMovies=require('./2-movies.cjs');

function moviesBasedOnOscarNominations(objectGiven){
    
    let result=Object.entries(objectGiven).filter((arrayComing)=>{
        return (arrayComing[1].totalEarnings).replace("$","").replace("M","")>500 && (arrayComing[1].oscarNominations>3)
    });
    
    return Object.fromEntries(result);
}

let result=moviesBasedOnOscarNominations(favouritesMovies);

console.log(result)